using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.StatusService
{
    public interface IStatusRepository
    {
        Task<ICollection<Status>> GetAllStatusesAsync();
        Task<Status> GetStatusAsync(int id);
        Task<Status> CreateStatusAsync(CreateStatusRequest request);
        Task UpdateStatusAsync(UpdateStatusRequest request);
        Task DeleteStatusAsync(int id);
    }
}