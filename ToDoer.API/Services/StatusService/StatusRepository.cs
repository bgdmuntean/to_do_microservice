using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Todoer.API.Services;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.CommandService.Status;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.StatusService
{
    public class StatusRepository : BaseRepository, IStatusRepository
    {
        private readonly IStatusCommandRepository _command;
        private readonly IMapper _mapper;
        public StatusRepository(IConfiguration configuration, IStatusCommandRepository command, IMapper mapper) : base(configuration)
        {
            _command = command;
            _mapper = mapper;
        }


        public async Task<Status> CreateStatusAsync(CreateStatusRequest request)
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryFirstOrDefaultAsync<Status>(_command.GetStatusByLabel, new {Label = request.Label});
                    if (query != null)
                    {
                        throw new HttpException(HttpStatusCode.Conflict, $"A status already exists with the same properties [{JsonConvert.SerializeObject(query)}]");
                    }

                    var status = _mapper.Map<Status>(request);
                    var id = await conn.ExecuteAsync(_command.AddStatus, status);

                    status = await conn.QueryFirstOrDefaultAsync<Status>(_command.GetStatusById, new {Id = id});
                    return status;
                }
            ).ConfigureAwait(false);


        public async Task DeleteStatusAsync(int id)
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryFirstOrDefaultAsync<Status>(_command.GetStatusById, new {Id = id});
                    if(query == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Status with id [{id}] was not found");
                    }
                    await conn.ExecuteAsync(_command.DeleteStatus, new {Id = id});
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<Status>> GetAllStatusesAsync()
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryAsync<Status>(_command.GetStatuses);
                    return query.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<Status> GetStatusAsync(int id)
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryFirstOrDefaultAsync<Status>(_command.GetStatusById, new {Id = id});
                    if(query == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Status with id [{id}] was not found.");
                    }
                    return query;
                }
            ).ConfigureAwait(false);


        public async Task UpdateStatusAsync(UpdateStatusRequest request)
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryFirstOrDefaultAsync<Status>(_command.GetStatusById, new {Id = request.Id});
                    if(query == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Status with id [{request.Id}] was not found");
                    }
                    var status = _mapper.Map<Status>(request);
                    await conn.ExecuteAsync(_command.UpdateStatus, status);
                }
            ).ConfigureAwait(false);
    }
}