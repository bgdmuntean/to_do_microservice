namespace ToDoer.API.Services.CommandService.Status
{
    public interface IStatusCommandRepository
    {
        string GetStatuses { get; }
        string GetStatusById { get; }
        string GetStatusByLabel { get; }
        string AddStatus { get; }
        string UpdateStatus { get; }
        string DeleteStatus { get; } 
        
    }
}