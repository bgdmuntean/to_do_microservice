namespace ToDoer.API.Services.CommandService.Task
{
    public class TaskCommandRepository : ITaskCommandRepository
    {
        public string GetTasks => "SELECT * FROM Task";
        public string GetTaskById => "SELECT * FROM Task WHERE Id = @Id";
        public string AddTask => "INSERT INTO Task (Label, Description, CreatedAt, ModifiedAt, Due, ListId, StatusId, CategoryId) VALUES(@Label, @Description, @CreatedAt, @ModifiedAt, @Due, @ListId, @StatusId, @CategoryId)";
        public string ChangeStatus => "UPDATE Task SET StatusId = @StatusId WHERE Id = @Id";
        public string UpdateTask => "UPDATE Task  SET Label = @Label, Description = @Description, ModifiedAt = @ModifiedAt, Due = @Due, ListId = @ListId, StatusId = @StatusId, CategoryId = @CategoryId WHERE Id = @Id";
        public string DeleteTask => "DELETE FROM Task WHERE Id = @Id";
        public string GetTasksByListId => "SELECT * FROM Task WHERE ListId = @ListId";
        public string GetTasksBetweenDates => "SELECT * FROM Task WHERE Due BETWEEN @StartDate AND @EndDate";
    }
}
