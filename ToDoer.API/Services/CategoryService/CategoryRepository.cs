using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Todoer.API.Services;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.CommandService.Category;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.CategoryService
{
    public class CategoryRepository : BaseRepository, ICategoryRepository
    {
        private readonly ICategoryCommandRepository _command;
        private readonly IMapper _mapper;
        public CategoryRepository(IConfiguration configuration, ICategoryCommandRepository command, IMapper mapper) : base(configuration)
        {
            _command = command;
            _mapper = mapper;
        }

        public async Task<Category> CreateCategoryAsync(CreateCategoryRequest request)
           => await WithConnection(async conn =>
            {
                var query = await conn.QueryFirstOrDefaultAsync<Category>(_command.GetCategoryByLabel, new {Label = request.Label}).ConfigureAwait(false);
                
                if(query != null)
                {
                    throw new HttpException(HttpStatusCode.Conflict, $"A category already exists with the same properties [{JsonConvert.SerializeObject(query)}]");
                }
                var category = _mapper.Map<Category>(request);
                var categoryId = await conn.ExecuteAsync(_command.AddCategory, category).ConfigureAwait(false);
                category = await conn.QueryFirstOrDefaultAsync<Category>(_command.GetCategoryById, new {Id = categoryId}).ConfigureAwait(false);
                return category;
            }).ConfigureAwait(false);


        public async Task DeleteCategoryAsync(int id)
            => await WithConnection(async conn =>
            {
                var query = await conn.QueryFirstOrDefaultAsync<Category>(_command.GetCategoryById, new {Id = id}).ConfigureAwait(false);
                if(query == null)
                {
                    throw new HttpException(HttpStatusCode.NotFound, $"Category with id [{id}] could not be found");
                }

                await conn.ExecuteAsync(_command.DeleteCategory, new { Id = id }).ConfigureAwait(false);
            }).ConfigureAwait(false);


        public async Task<ICollection<Category>> GetAllCategoriesAsync()
            => await WithConnection(async conn =>
                {
                    var query = await conn.QueryAsync<Category>(_command.GetCategories).ConfigureAwait(false);
                    return query.AsList();
                }).ConfigureAwait(false);
        

        public async Task<Category> GetCategoryAsync(int id)
            => await WithConnection(async conn =>
                {
                    var query = await conn.QueryFirstOrDefaultAsync<Category>(_command.GetCategoryById, new { Id = id }).ConfigureAwait(false);
                    if(query == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Category with id [{id}] could not be found");
                    }
                    return query;
                }).ConfigureAwait(false);


        public async Task UpdateCategoryAsync(UpdateCategoryRequest request)
            => await WithConnection(async conn =>
            {
                var query = await conn.QueryFirstOrDefaultAsync<Category>(_command.GetCategoryById, new { Id = request.Id }).ConfigureAwait(false);
                if(query == null)
                {
                    throw new HttpException(HttpStatusCode.NotFound, $"Category with id [{request.Id}] could not be found");
                }

                var category = _mapper.Map<Category>(request);
                await conn.ExecuteAsync(_command.UpdateCategory, category).ConfigureAwait(false);
            }).ConfigureAwait(false);
    }
}