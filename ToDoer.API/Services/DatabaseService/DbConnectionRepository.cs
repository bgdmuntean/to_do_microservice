using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ToDoer.API.Services.DatabaseService
{
    public class DbConnectionRepository : IDbConnectionRepository
    {
        private readonly string _connectionString;

        public DbConnectionRepository(string connectionString)
        {
            if(string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }
            _connectionString = connectionString;
        }

        public async Task<IDbConnection> CreateConnectionAsync()
        {
            var sqlConnection = new SqlConnection(_connectionString);
            await sqlConnection.OpenAsync().ConfigureAwait(false);
            return sqlConnection;
        }
    }
}