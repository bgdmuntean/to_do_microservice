using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Requests;
using EntityTask = ToDoer.API.Domain.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.TaskService
{
    public interface ITaskRepository
    {
        Task<ICollection<EntityTask>> GetAllTasksAsync();
        Task<ICollection<EntityTask>> GetTasksFilteredAsync(FilterTaskRequest request);
        Task<EntityTask> GetTaskAsync(int id);
        Task UpdateTaskStatusAsync(int id, int statusId);
        Task<ICollection<EntityTask>> GetTasksByListIdAsync(int id);
        Task<EntityTask> CreateTaskAsync(CreateTaskRequest request);
        Task<ICollection<EntityTask>> CreateTasksAsync(ICollection<CreateTaskRequest> request);
        Task UpdateTaskAsync(UpdateTaskRequest request);
        Task DeleteTaskAsync(int id);
    }
}