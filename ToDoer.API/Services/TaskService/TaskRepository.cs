using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Todoer.API.Services;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.CommandService.List;
using ToDoer.API.Services.CommandService.Task;
using ToDoer.API.Utils.Extensions;

namespace ToDoer.API.Services.TaskService
{
    public class TaskRepository : BaseRepository, ITaskRepository
    {
        private readonly ITaskCommandRepository _command;
        private readonly IListCommandRepository _listCommand;
        private readonly IMapper _mapper;

        public TaskRepository(IConfiguration configuration, ITaskCommandRepository commandRepository, IListCommandRepository listCommandRepository, IMapper mapper) : base(configuration)
        {
            _command = commandRepository;
            _listCommand = listCommandRepository;
            _mapper = mapper;
        }


        public async Task<Domain.Models.Task> CreateTaskAsync(CreateTaskRequest request)
            => await WithConnection(async conn => 
                {
                    var list = await conn.QueryFirstOrDefaultAsync<List>(_listCommand.GetListById, new {Id = request.ListId}).ConfigureAwait(false);
                    
                    CanAddTask(request, list);

                    var task = _mapper.Map<Domain.Models.Task>(request);

                    task.CreatedAt = DateTimeOffset.UtcNow;
                    task.ModifiedAt = DateTimeOffset.UtcNow;

                    var id = await conn.ExecuteAsync(_command.AddTask, task);

                    task = await conn.QueryFirstOrDefaultAsync<Domain.Models.Task>(_command.GetTaskById, new {Id = id}).ConfigureAwait(false);
                    return task;
                }
            );


        public async Task<ICollection<Domain.Models.Task>> CreateTasksAsync(ICollection<CreateTaskRequest> request)
            => await WithConnection(async conn => 
                {
                    var tasks = new List<Domain.Models.Task>();
                    foreach(var taskRequest in request)
                    {
                        var task = _mapper.Map<Domain.Models.Task>(request);

                        task.CreatedAt = DateTimeOffset.UtcNow;
                        task.ModifiedAt = DateTimeOffset.UtcNow;

                        var id = await conn.ExecuteAsync(_command.AddTask, task);
                        tasks.Add(await conn.QueryFirstOrDefaultAsync<Domain.Models.Task>(_command.GetTaskById, new {Id = id}).ConfigureAwait(false));
                    }
                    return tasks;
                }
            ).ConfigureAwait(false);


        public async System.Threading.Tasks.Task DeleteTaskAsync(int id)
            => await WithConnection(async conn => 
                {
                    var task = await conn.QueryFirstOrDefaultAsync<Domain.Models.Task>(_command.GetTaskById, new {Id = id}).ConfigureAwait(false);
                    if(task == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Task with id [{id}] was not found");
                    }

                    await conn.ExecuteAsync(_command.DeleteTask, new {Id = id}).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<Domain.Models.Task>> GetAllTasksAsync()
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryAsync<Domain.Models.Task>(_command.GetTasks).ConfigureAwait(false);
                    return query.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<Domain.Models.Task> GetTaskAsync(int id)
            => await WithConnection(async conn => 
                {
                   var task = await conn.QueryFirstOrDefaultAsync<Domain.Models.Task>(_command.GetTaskById, new {Id = id}).ConfigureAwait(false);
                    if(task == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Task with id [{id}] was not found");
                    }
                    return task;
                }
            ).ConfigureAwait(false);

        public async Task<ICollection<Domain.Models.Task>> GetTasksByListIdAsync(int id)
            => await WithConnection(async conn => 
                {
                   var task = await conn.QueryAsync<Domain.Models.Task>(_command.GetTasksByListId, new {ListId = id}).ConfigureAwait(false);
                    if(task.AsList().Count == 0)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"There are no tasks with ListId[{id}]");
                    }
                    return task.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<Domain.Models.Task>> GetTasksFilteredAsync(FilterTaskRequest request)
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryAsync<Domain.Models.Task>(_command.GetTasksBetweenDates, request).ConfigureAwait(false);
                    return query.AsList();
                }
            ).ConfigureAwait(false);

        public async System.Threading.Tasks.Task UpdateTaskAsync(UpdateTaskRequest request)
            => await WithConnection(async conn => 
                {
                    var task = await conn.QueryFirstOrDefaultAsync<Domain.Models.Task>(_command.GetTaskById, new {Id = request.Id}).ConfigureAwait(false);

                    if(task == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Task with id [{request.Id}] was not found");
                    }
                    task = _mapper.Map<Domain.Models.Task>(request);
                    task.ModifiedAt = DateTimeOffset.UtcNow;
                    await conn.ExecuteAsync(_command.UpdateTask, task).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);


        public async System.Threading.Tasks.Task UpdateTaskStatusAsync(int id, int statusId)
            => await WithConnection(async conn => 
                {
                    var task = await conn.QueryFirstOrDefaultAsync<Domain.Models.Task>(_command.GetTaskById, new {Id = id}).ConfigureAwait(false);
                    if(task == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Task with id [{id}] was not found.");
                    }

                    await conn.ExecuteAsync(_command.ChangeStatus, new {Id = id, StatusId = statusId }).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);


        private static void CanAddTask(CreateTaskRequest request, List list)
        {
            if(list == null)
            {
                throw new HttpException(HttpStatusCode.NotFound, $"List with id [{request.ListId}] was not found");
            }
            if(!request.IsValid(list))
            {
                throw new HttpException(HttpStatusCode.Forbidden, $"The due date of a task cannot be later than the due date of a list (Task: [{JsonConvert.SerializeObject(request)}], List: [{JsonConvert.SerializeObject(new{ Label = list.Label, Due = list.Due, CreatedAt = list.CreatedAt, ModifiedAt = list.ModifiedAt})}])");
            }
        }
    }
}