using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.ListService
{
    public interface IListRepository
    {
        Task<ICollection<List>> GetAllListsAsync();
        Task<List> GetListAsync(int id);
        Task<List> CreateListAsync(CreateListRequest request);
        Task<ICollection<List>> GetListsFilteredAsync(FilterListRequest request);
        Task UpdateListAsync(UpdateListRequest request);
        Task DeleteListAsync(int id);
    }
}