using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Extensions.Configuration;
using Todoer.API.Services;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.CommandService.List;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.ListService
{
    public class ListRepository : BaseRepository, IListRepository
    {
        private readonly IMapper _mapper;
        private readonly IListCommandRepository _command;
        public ListRepository(IConfiguration configuration, IListCommandRepository command, IMapper mapper) : base(configuration)
        {
            _mapper = mapper;
            _command = command;
        }


        public async Task<List> CreateListAsync(CreateListRequest request)
            => await WithConnection(async conn => 
                {
                    var list = _mapper.Map<List>(request);
                    list.CreatedAt = DateTimeOffset.UtcNow;
                    list.ModifiedAt = DateTimeOffset.UtcNow;
                    var id = await conn.ExecuteAsync(_command.AddList, list).ConfigureAwait(false);

                    list = await conn.QueryFirstOrDefaultAsync<List>(_command.GetListById, new {Id = id}).ConfigureAwait(false);
                    return list;
                }
            ).ConfigureAwait(false);


        public async Task DeleteListAsync(int id)
            => await WithConnection(async conn => 
                {
                    var list = await conn.QueryFirstOrDefaultAsync<List>(_command.GetListById, new {Id = id}).ConfigureAwait(false);

                    if(list == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"List with id [{id}] was not found");
                    }
                    await conn.ExecuteAsync(_command.DeleteList, new {Id = id}).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<List>> GetAllListsAsync()
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryAsync<List>(_command.GetLists).ConfigureAwait(false);
                    return query.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<List> GetListAsync(int id)
            => await WithConnection(async conn => 
                {
                    var list = await conn.QueryFirstOrDefaultAsync<List>(_command.GetListById, new {Id = id}).ConfigureAwait(false);

                    if(list == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"List with id [{id}] was not found");
                    }
                    return list;
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<List>> GetListsFilteredAsync(FilterListRequest request)
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryAsync<List>(_command.GetListsBetweenDates, request).ConfigureAwait(false);
                    return query.AsList();
                }
            ).ConfigureAwait(false);


        public async Task UpdateListAsync(UpdateListRequest request)
            => await WithConnection(async conn => 
                {
                    var list = await conn.QueryFirstOrDefaultAsync<List>(_command.GetListById, new {Id = request.Id}).ConfigureAwait(false);

                    if(list == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"List with id [{request.Id}] was not found");
                    }
                    list = _mapper.Map<List>(request);
                    list.ModifiedAt = DateTimeOffset.UtcNow;
                    await conn.ExecuteAsync(_command.UpdateList, list).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);
    }
}