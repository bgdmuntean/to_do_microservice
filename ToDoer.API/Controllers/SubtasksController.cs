using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.SubtaskService;

namespace ToDoer.API.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class SubtasksController : ControllerBase
    {
        private readonly ISubtaskRepository _subtaskRepository;

        public SubtasksController(ISubtaskRepository subtaskRepository)
        {
            _subtaskRepository = subtaskRepository;
        }


        /// <summary>
        /// Gets all the subtasks
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/subtasks
        /// 
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "taskId": 1,
        ///                 "statusId": 1
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of subtasks</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Subtask>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Subtask>>> GetSubtasks()
            => Ok(await _subtaskRepository.GetAllSubtasksAsync().ConfigureAwait(false));


        
        /// <summary>
        /// Gets subtasks filtered
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/subtasks/filtered
        ///     Body:
        ///     {
        ///         "startDate": "2020-11-15T15:20:51.699Z",
        ///         "endDate": "2020-11-15T15:20:51.699Z"
        ///     }
        ///
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "taskId": 1,
        ///                 "statusId": 1
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of subtasks filtered by date</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost("filtered")]
        [ProducesResponseType(typeof(ICollection<Subtask>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Subtask>>> GetSubtasksFiltered([FromBody, BindRequired] FilterSubtaskRequest request)
            => Ok(await _subtaskRepository.GetSubtasksFilteredAsync(request).ConfigureAwait(false));


        
        /// <summary>
        /// Gets a subtask by ID
        /// </summary>
        /// <param name="id">
        /// Subtask ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/subtasks/{id}
        /// 
        /// Sample response:
        /// 
        ///          {
        ///              "id": 1,
        ///              "label": "Item1",
        ///              "description": "some description",
        ///              "createdAt": "2020-11-15T15:20:51.699Z",
        ///              "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///              "taskId": 1,
        ///              "statusId": 1
        ///              "due": "2020-11-15T15:20:51.699Z"
        ///          }
        ///
        /// </remarks>
        /// <response code="200">Returns the subtask</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(Subtask), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Subtask>> GetSubtask([FromRoute, BindRequired] int id)
            => Ok(await _subtaskRepository.GetSubtaskAsync(id).ConfigureAwait(false));


        
        /// <summary>
        /// Gets the subtasks associated with a task
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/subtasks/{id}/taskId
        /// 
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "taskId": 1,
        ///                 "statusId": 1
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of subtasks belonging to a task</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet("{taskId:int}/taskId")]
        [ProducesResponseType(typeof(ICollection<Subtask>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Subtask>>> GetSubtasksByTaskId([FromRoute, BindRequired] int taskId)
            => Ok(await _subtaskRepository.GetSubtaskByTaskIdAsync(taskId).ConfigureAwait(false));



        /// <summary>
        /// Deletes a subtask by ID
        /// </summary>
        /// <param name="id">
        /// Subtask ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE api/subtasks/{id}
        /// 
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - Operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpDelete("{id:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> DeleteSubtask([FromRoute, BindRequired] int id)
        {
            await _subtaskRepository.DeleteSubtaskAsync(id).ConfigureAwait(false);
            return NoContent();
        }


        /// <summary>
        /// Creates a subtask
        /// </summary>
        /// <param name="subtaskRequest">
        /// Create Subtask request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/subtasks
        ///     
        ///     Body:
        ///       {
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z",
        ///           "taksId": 1,
        ///           "statusId": 1,
        ///       }
        ///
        /// Sample response:
        /// 
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "createdAt": "2020-11-15T15:20:51.699Z",
        ///           "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///           "taskId": 1,
        ///           "statusId": 1
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///
        /// </remarks>
        /// <response code="201">Newly created entity</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="409">If the item exists already</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost]
        [ProducesResponseType(typeof(Domain.Models.Task), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Subtask>> CreateSubtask([FromBody, BindRequired] CreateSubtaskRequest subtaskRequest)
             => Created(string.Empty, await _subtaskRepository.CreateSubtaskAsync(subtaskRequest).ConfigureAwait(false));


        
        /// <summary>
        /// Creates subtasks based on a collection of requests
        /// </summary>
        /// <param name="subtaskRequests">
        /// Collection of Create Subtask requests
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///  POST api/subtasks/bulk
        ///  Body:
        ///
        ///   {
        ///      [
        ///         {
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z",
        ///           "taskId": 1,
        ///           "statusId": 1,
        ///         }
        ///     ]
        ///   }
        ///
        /// Sample response:
        ///
        /// {
        ///    [
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "createdAt": "2020-11-15T15:20:51.699Z",
        ///           "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///           "taskId": 1,
        ///           "statusId": 1
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///   ]
        /// }
        ///
        ///
        /// </remarks>
        /// <response code="201">Newly created entities</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost("bulk")]
        [ProducesResponseType(typeof(ICollection<Subtask>), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Subtask>> CreateSubtasks([FromBody, BindRequired] ICollection<CreateSubtaskRequest> subtaskRequests)
             => Created(string.Empty, await _subtaskRepository.CreateSubtasksAsync(subtaskRequests).ConfigureAwait(false));

            
        
        /// <summary>
        /// Updates a subtask
        /// </summary>
        /// <param name="subtaskRequest">
        /// Update Subtask request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/subtask
        ///     Body:
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z",
        ///           "taskId": 1,
        ///           "statusId": 1,
        ///       }
        ///
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> UpdateTask([FromBody, BindRequired] UpdateSubtaskRequest subtaskRequest)
        {
            await _subtaskRepository.UpdateSubtaskAsync(subtaskRequest).ConfigureAwait(false);
            return NoContent();
        }


        
        /// <summary>
        /// Updates the status of a subtask
        /// </summary>
        /// <param name="id">
        /// Subtask id
        /// </param>
        /// <param name="statusId">
        /// The Status Id
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PATCH api/subtasks/{id}
        ///     Query:
        ///     statusId    
        /// 
        ///
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPatch("{id:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> UpdateSubtaskStatus([FromRoute, BindRequired] int id, [FromQuery, BindRequired] int statusId)
        {
            await _subtaskRepository.UpdateSubtaskStatusAsync(id, statusId).ConfigureAwait(false);
            return NoContent();
        }
    }
}