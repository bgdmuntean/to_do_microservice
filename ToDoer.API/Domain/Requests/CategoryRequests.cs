namespace ToDoer.API.Domain.Requests
{
    public class CreateCategoryRequest
    {
        public string Label { get; set; }
    }

    public class UpdateCategoryRequest
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }
}