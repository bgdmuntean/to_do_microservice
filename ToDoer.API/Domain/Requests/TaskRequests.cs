using System;

namespace ToDoer.API.Domain.Requests
{
    public class CreateTaskRequest
    {
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
        public int ListId {get;set;}
        public int StatusId {get;set;}
        public int CategoryId {get;set;}
    }

    public class UpdateTaskRequest
    {
        public int Id {get;set;}
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
        public int ListId {get;set;}
        public int StatusId {get;set;}
        public int CategoryId {get;set;}
    }

    public class FilterTaskRequest 
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}