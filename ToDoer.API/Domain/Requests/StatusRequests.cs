namespace ToDoer.API.Domain.Requests
{
    public class CreateStatusRequest
    {
        public string Label { get; set; }
    }

    public class UpdateStatusRequest
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }
}