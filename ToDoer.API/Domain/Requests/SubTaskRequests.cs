using System;

namespace ToDoer.API.Domain.Requests
{
    public class CreateSubtaskRequest
    {
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
        public int TaskId {get;set;}
        public int StatusId {get;set;}
    }

    public class UpdateSubtaskRequest
    {
        public int Id {get;set;}
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
        public int TaskId {get;set;}
        public int StatusId {get;set;}
    }

    public class FilterSubtaskRequest
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}