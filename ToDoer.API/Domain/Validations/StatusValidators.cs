using FluentValidation;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Domain.Validations
{
    public class CreateStatusRequestValidator : AbstractValidator<CreateStatusRequest>
    {
        public CreateStatusRequestValidator()
        {
            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateStatusRequest).ToString(), "Label", "being null or empty"));
        }
    }

    public class UpdatStatusRequestValidator : AbstractValidator<UpdateStatusRequest>
    {
        public UpdatStatusRequestValidator()
        {
            RuleFor(_ => _.Id)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateStatusRequest).ToString(), "Id", "being null or empty"));

            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateStatusRequest).ToString(), "Label", "being null or empty"));
        }
    }
}