using System;
using FluentValidation;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Domain.Validations
{
    public class CreateSubtaskRequestValidator : AbstractValidator<CreateSubtaskRequest>
    {
        public CreateSubtaskRequestValidator()
        {
            RuleFor(_ => _.TaskId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "TaskId", "being null or empty"));

            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Due", "being in the past"));
        }
    }


    public class UpdateSubtaskRequestValidator : AbstractValidator<UpdateSubtaskRequest>
    {
        public UpdateSubtaskRequestValidator()
        {
            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Id", "being null or empty"));

            RuleFor(_ => _.TaskId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "TaskId", "being null or empty"));

            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskRequest), "Due", "being in the past"));
        }
    }


        public class FilterSubtaskRequestValidator : AbstractValidator<FilterSubtaskRequest>
    {
        public FilterSubtaskRequestValidator()
        {
            
            RuleFor(_ => _.StartDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskRequest), "StartDate", "being null or empty"))
                .GreaterThanOrEqualTo(_ => _.EndDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskRequest), "StartDate", "being greatar than EndDate"));

            RuleFor(_ => _.EndDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskRequest), "EndDate", "being null or empty"))
                .LessThanOrEqualTo(_ => _.StartDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskRequest), "EndDate", "being smaller than StartDate"));
        }
    }
}