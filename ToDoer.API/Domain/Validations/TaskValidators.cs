using System;
using FluentValidation;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Domain.Validations
{
    public class CreateTaskRequestValidator : AbstractValidator<CreateTaskRequest>
    {
        public CreateTaskRequestValidator()
        {
            RuleFor(_ => _.ListId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskRequest), "ListId", "being null or empty"));

            RuleFor(_ => _.CategoryId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskRequest), "CategoryId", "being null or empty"));

            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskRequest), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskRequest), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskRequest), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskRequest), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskRequest), "Due", "being in the past"));
        }
    }


    public class UpdateTaskRequestValidator : AbstractValidator<UpdateTaskRequest>
    {
        public UpdateTaskRequestValidator()
        {
            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "Id", "being null or empty"));

            RuleFor(_ => _.ListId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "ListId", "being null or empty"));

            RuleFor(_ => _.CategoryId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "CategoryId", "being null or empty"));
            
            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskRequest), "Due", "being in the past"));
        }
    }

        public class FilterTaskRequestValidator : AbstractValidator<FilterTaskRequest>
    {
        public FilterTaskRequestValidator()
        {
            RuleFor(_ => _.StartDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskRequest), "StartDate", "being null or empty"))
                .LessThanOrEqualTo(_ => _.EndDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskRequest), "StartDate", "being greater than EndDate"));


              RuleFor(_ => _.StartDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskRequest), "EndDate", "being null or empty"))
                .GreaterThanOrEqualTo(_ => _.StartDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskRequest), "EndDate", "being smaller than StartDate"));
        }
    }
}