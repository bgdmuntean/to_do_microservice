using FluentValidation;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Domain.Validations
{
    public class CreateCategoryRequestValidator: AbstractValidator<CreateCategoryRequest>
    {
        public CreateCategoryRequestValidator()
        {
            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateCategoryRequest).ToString(), "Label", "being null or empty"));
        }
    }

    public class UpdateCategoryRequestValidator : AbstractValidator<UpdateCategoryRequest>
    {
        public UpdateCategoryRequestValidator()
        {
            RuleFor(_ => _.Id)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateCategoryRequest).ToString(), "Id", "being null or empty"));

            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateCategoryRequest).ToString(), "Label", "being null or empty"));
        }
    }
}