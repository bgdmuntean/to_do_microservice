using FluentValidation;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Domain.Validations
{
    public class CreateListRequestValidator : AbstractValidator<CreateListRequest>
    {
        public CreateListRequestValidator()
        {
            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateListRequest).ToString(), "Label", "being null or empty"));

             RuleFor(_ => _.Description)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateListRequest).ToString(), "Description", "being null or empty"));

             RuleFor(_ => _.Due)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateListRequest).ToString(), "Due", "being null or empty"));
        }
    }

    public class UpdateListRequestValidator : AbstractValidator<UpdateListRequest>
    {
        public UpdateListRequestValidator()
        {
            RuleFor(_ => _.Id)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateCategoryRequest).ToString(), "Id", "being null or empty"));

            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateCategoryRequest).ToString(), "Label", "being null or empty"));
        }
    }

    public class FilterListRequestValidator : AbstractValidator<FilterListRequest>
    {
        public FilterListRequestValidator()
        {
            RuleFor(_ => _.StartDate)
            .NotEmpty()
            .LessThanOrEqualTo(_ => _.EndDate) 
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(FilterListRequest).ToString(), "StartDate", "being null or empty"));

            RuleFor(_ => _.EndDate)
            .NotEmpty()
            .GreaterThanOrEqualTo(_ => _.StartDate) 
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(FilterListRequest).ToString(), "EndDate", "being null or empty"));

        }
    }
}