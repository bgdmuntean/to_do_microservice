using Microsoft.AspNetCore.Builder;
using Todoer.API.Utils.Middleware;

namespace ToDoer.API.Utils.Extensions
{
    public static class ExceptionHandlingExtensions
    {
        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            return app;
        }
    }
}