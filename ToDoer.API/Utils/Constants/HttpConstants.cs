namespace ToDoer.API.Utils.Constants
{
    public partial class HttpConstants
    {
        public static class StatusCodes
        {
            public static string BadRequest = "400";
            public static string NotFound = "404";
            public static string Ok = "200";
            public static string NoContent = "204";
            public static string Forbidden = "403";
            public static string Conflict = "409";
            public static string Unauthorized = "401";
            public static string InternalError = "500";
        }
    }
}