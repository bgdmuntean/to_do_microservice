using AutoMapper;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;

namespace ToDoer.API.Utils.Configurations
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateCategoryRequest, Category>();
            CreateMap<UpdateCategoryRequest, Category>();

            CreateMap<CreateListRequest, List>();
            CreateMap<UpdateListRequest, List>();

            CreateMap<CreateStatusRequest, Status>();
            CreateMap<UpdateStatusRequest, Status>();

            CreateMap<CreateSubtaskRequest, Subtask>();
            CreateMap<UpdateSubtaskRequest, Subtask>();

            CreateMap<CreateTaskRequest, Task>();
            CreateMap<UpdateTaskRequest, Task>();
        }
    }
}