using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.CategoryService;

namespace ToDoer.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        /// <summary>
        /// Gets all the categories
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/categories
        /// 
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "Label": "Item1",
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of categories</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Category>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Category>>> GetCategories()
            => Ok(await _categoryRepository.GetAllCategoriesAsync().ConfigureAwait(false));



        /// <summary>
        /// Gets a category by category ID
        /// </summary>
        /// <param name="id">
        /// Category ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/categories/{id}
        /// 
        /// Sample response:
        /// 
        ///     {
        ///        "id": 1,
        ///        "label": "Item1",
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns the category</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(Category), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Category>> GetCategory([FromRoute, BindRequired] int id)
            => Ok(await _categoryRepository.GetCategoryAsync(id).ConfigureAwait(false));


        /// <summary>
        /// Deletes a category by category ID
        /// </summary>
        /// <param name="id">
        /// Category ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE api/categories/{id}
        /// 
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - Operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpDelete("{id:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> DeleteCategory([FromRoute, BindRequired] int id)
        {
            await _categoryRepository.DeleteCategoryAsync(id).ConfigureAwait(false);
            return NoContent();
        }


        /// <summary>
        /// Creates a category
        /// </summary>
        /// <param name="categoryRequest">
        /// Create Category request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/categories
        ///     
        ///     Body:
        ///     {
        ///         "label": string
        ///     }
        ///
        /// Sample response:
        /// 
        ///    {
        ///         "label": "test"
        ///         "id": 1
        ///    } 
        ///
        /// </remarks>
        /// <response code="201">Newly created entity</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="409">If the item exists already</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost]
        [ProducesResponseType(typeof(Category), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Category>> CreateCategory([FromBody, BindRequired] CreateCategoryRequest categoryRequest)
             => Created(string.Empty, await _categoryRepository.CreateCategoryAsync(categoryRequest).ConfigureAwait(false));



        /// <summary>
        /// Updates a category
        /// </summary>
        /// <param name="categoryRequest">
        /// Update Category request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/categories
        ///     
        ///     Body:
        ///     {
        ///         "id": 1
        ///         "label": "something"
        ///     }
        ///
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> UpdateCategory([FromBody, BindRequired] UpdateCategoryRequest categoryRequest)
        {
            await _categoryRepository.UpdateCategoryAsync(categoryRequest).ConfigureAwait(false);
            return NoContent();
        }
    }
}