using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.ListService;

namespace ToDoer.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ListsController : ControllerBase
    {
        private readonly IListRepository _listRepository;

        public ListsController(IListRepository listRepository)
        {
            _listRepository = listRepository;
        }


        /// <summary>
        /// Gets all the lists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/lists
        /// 
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of lists</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet]
        [ProducesResponseType(typeof(ICollection<List>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<List>>> GetLists()
            => Ok(await _listRepository.GetAllListsAsync().ConfigureAwait(false));



        /// <summary>
        /// Gets a list by list ID
        /// </summary>
        /// <param name="id">
        /// List ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/lists/{id}
        /// 
        /// Sample response:
        /// 
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "createdAt": "2020-11-15T15:20:51.699Z",
        ///           "modifiedAt":"2020-11-15T15:20:51.699Z",
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///
        /// </remarks>
        /// <response code="200">Returns the list</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(List), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<List>> GetList([FromRoute, BindRequired] int id)
            => Ok(await _listRepository.GetListAsync(id).ConfigureAwait(false));



        /// <summary>
        /// Deletes a list by list ID
        /// </summary>
        /// <param name="id">
        /// List ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE api/lists/{id}
        /// 
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - Operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpDelete("{id:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> DeleteList([FromRoute, BindRequired] int id)
        {
            await _listRepository.DeleteListAsync(id).ConfigureAwait(false);
            return NoContent();
        }


        /// <summary>
        /// Creates a list
        /// </summary>
        /// <param name="listRequest">
        /// Create list request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/list
        ///     
        ///     Body:
        ///       {
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///
        /// Sample response:
        /// 
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "createdAt": "2020-11-15T15:20:51.699Z",
        ///           "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///
        /// </remarks>
        /// <response code="201">Newly created entity</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="409">If the item exists already</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost]
        [ProducesResponseType(typeof(List), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<List>> CreateList([FromBody, BindRequired] CreateListRequest listRequest)
             => Created(string.Empty, await _listRepository.CreateListAsync(listRequest).ConfigureAwait(false));


         /// <summary>
        /// Updates a list
        /// </summary>
        /// <param name="listRequest">
        /// Update List request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/lists
        ///     
        ///     Body:
        ///       {
        ///           "id": "Item1",
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> UpdateList([FromBody, BindRequired] UpdateListRequest listRequest)
        {
            await _listRepository.UpdateListAsync(listRequest).ConfigureAwait(false);
            return NoContent();
        }


        /// <summary>
        /// Gets lists filtered
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/lists/filtered
        ///     Body:
        ///     {
        ///         "startDate": "2020-11-15T15:20:51.699Z",
        ///         "endDate": "2020-11-15T15:20:51.699Z"
        ///     }
        ///
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of lists</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost("filtered")]
        [ProducesResponseType(typeof(ICollection<List>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<List>>> GetListsFiltered([FromBody, BindRequired] FilterListRequest request)
            => Ok(await _listRepository.GetListsFilteredAsync(request).ConfigureAwait(false));

    }
}