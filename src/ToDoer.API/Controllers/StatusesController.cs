using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.StatusService;

namespace ToDoer.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class StatusesController : ControllerBase
    {
        private readonly IStatusRepository _statusRepository;

        public StatusesController(IStatusRepository statusRepository)
        {
            _statusRepository = statusRepository;
        }


        /// <summary>
        /// Gets all the statuses
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/statuses
        /// 
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of statuses</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Status>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Status>>> GetStatuses()
            => Ok(await _statusRepository.GetAllStatusesAsync().ConfigureAwait(false));


        /// <summary>
        /// Gets a status by status ID
        /// </summary>
        /// <param name="id">
        /// Status ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/statuses/{id}
        /// 
        /// Sample response:
        /// 
        ///     {
        ///        "id": 1,
        ///        "label": "Item1",
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns the status</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(Status), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Status>> GetStatus([FromRoute, BindRequired] int id)
            => Ok(await _statusRepository.GetStatusAsync(id).ConfigureAwait(false));


        /// <summary>
        /// Deletes a status by status ID
        /// </summary>
        /// <param name="id">
        /// Status ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE api/status/{id}
        /// 
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - Operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpDelete("{id:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> DeleteStatus([FromRoute, BindRequired] int id)
        {
            await _statusRepository.DeleteStatusAsync(id).ConfigureAwait(false);
            return NoContent();
        }


        /// <summary>
        /// Creates a status
        /// </summary>
        /// <param name="statusRequest">
        /// Create Status request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/statuses
        ///     
        ///     Body:
        ///     {
        ///         "label": string
        ///     }
        ///
        /// Sample response:
        /// 
        ///    {
        ///         "label": "test"
        ///         "id": 1
        ///    } 
        ///
        /// </remarks>
        /// <response code="201">Newly created entity</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="409">If the item exists already</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost]
        [ProducesResponseType(typeof(Status), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Status>> CreateStatus([FromBody, BindRequired] CreateStatusRequest statusRequest)
             => Created(string.Empty, await _statusRepository.CreateStatusAsync(statusRequest).ConfigureAwait(false));

    
    
        /// <summary>
        /// Updates a status
        /// </summary>
        /// <param name="statusRequest">
        /// Update Status request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/statuses
        ///     
        ///     Body:
        ///     {
        ///         "id": 1
        ///         "label": "something"
        ///     }
        ///
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> UpdateStatus([FromBody, BindRequired] UpdateStatusRequest statusRequest)
        {
            await _statusRepository.UpdateStatusAsync(statusRequest).ConfigureAwait(false);
            return NoContent();
        }

    }
}