using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.TaskService;

namespace ToDoer.API.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskRepository _taskRepository;

        public TasksController(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }


        /// <summary>
        /// Gets all the tasks
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/tasks
        /// 
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "listId": 1,
        ///                 "categoryId": 1,
        ///                 "statusId": 1
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of tasks</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Domain.Models.Task>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Domain.Models.Task>>> GetTasks()
            => Ok(await _taskRepository.GetAllTasksAsync().ConfigureAwait(false));


        /// <summary>
        /// Gets tasks filtered
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/tasks/filtered
        ///     Body:
        ///     {
        ///         "startDate": "2020-11-15T15:20:51.699Z",
        ///         "endDate": "2020-11-15T15:20:51.699Z"
        ///     }
        ///
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "listId": 1,
        ///                 "categoryId": 1,
        ///                 "statusId": 1
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of tasks filtered by date</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost("filtered")]
        [ProducesResponseType(typeof(ICollection<Domain.Models.Task>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Domain.Models.Task>>> GetTasksFiltered([FromBody, BindRequired] FilterTaskRequest request)
            => Ok(await _taskRepository.GetTasksFilteredAsync(request).ConfigureAwait(false));


        /// <summary>
        /// Gets a task by task ID
        /// </summary>
        /// <param name="id">
        /// Task ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/tasks/{id}
        /// 
        /// Sample response:
        /// 
        ///          {
        ///              "id": 1,
        ///              "label": "Item1",
        ///              "description": "some description",
        ///              "createdAt": "2020-11-15T15:20:51.699Z",
        ///              "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///              "listId": 1,
        ///              "categoryId": 1,
        ///              "statusId": 1
        ///              "due": "2020-11-15T15:20:51.699Z"
        ///          }
        ///
        /// </remarks>
        /// <response code="200">Returns the task</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(Domain.Models.Task), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Domain.Models.Task>> GetTask([FromRoute, BindRequired] int id)
            => Ok(await _taskRepository.GetTaskAsync(id).ConfigureAwait(false));


        /// <summary>
        /// Gets the tasks associated with a list
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/tasks/{id}/listId
        /// 
        /// Sample response:
        ///
        ///     {
        ///         [   
        ///             {
        ///                 "id": 1,
        ///                 "label": "Item1",
        ///                 "description": "some description",
        ///                 "createdAt": "2020-11-15T15:20:51.699Z",
        ///                 "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///                 "listId": 1,
        ///                 "categoryId": 1,
        ///                 "statusId": 1
        ///                 "due": "2020-11-15T15:20:51.699Z"
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Returns a collection of tasks belonging to a list</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpGet("{listId:int}/listId")]
        [ProducesResponseType(typeof(ICollection<Domain.Models.Task>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<ICollection<Domain.Models.Task>>> GetTasksByListId([FromRoute, BindRequired] int listId)
            => Ok(await _taskRepository.GetTasksByListIdAsync(listId).ConfigureAwait(false));


        /// <summary>
        /// Deletes a task by task ID
        /// </summary>
        /// <param name="id">
        /// Task ID
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE api/tasks/{id}
        /// 
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - Operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpDelete("{id:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> DeleteTask([FromRoute, BindRequired] int id)
        {
            await _taskRepository.DeleteTaskAsync(id).ConfigureAwait(false);
            return NoContent();
        }


        /// <summary>
        /// Creates a task
        /// </summary>
        /// <param name="taskRequest">
        /// Create Task request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/tasks
        ///     
        ///     Body:
        ///       {
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z",
        ///           "listId": 1,
        ///           "statusId": 1,
        ///           "categoryId": 1
        ///       }
        ///
        /// Sample response:
        /// 
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "createdAt": "2020-11-15T15:20:51.699Z",
        ///           "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///           "listId": 1,
        ///           "categoryId": 1,
        ///           "statusId": 1
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///
        /// </remarks>
        /// <response code="201">Newly created entity</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="409">If the item exists already</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost]
        [ProducesResponseType(typeof(Domain.Models.Task), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Domain.Models.Task>> CreateTask([FromBody, BindRequired] CreateTaskRequest taskRequest)
             => Created(string.Empty, await _taskRepository.CreateTaskAsync(taskRequest).ConfigureAwait(false));


        /// <summary>
        /// Creates tasks based on a collection of requests
        /// </summary>
        /// <param name="taskRequests">
        /// Collection of Create Task requests
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///  POST api/tasks/bulk
        ///  Body:
        ///
        ///   {
        ///      [
        ///         {
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z",
        ///           "listId": 1,
        ///           "statusId": 1,
        ///           "categoryId": 1
        ///         }
        ///     ]
        ///   }
        ///
        /// Sample response:
        ///
        /// {
        ///    [
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "createdAt": "2020-11-15T15:20:51.699Z",
        ///           "modifiedAt": "2020-11-15T15:20:51.699Z",
        ///           "listId": 1,
        ///           "categoryId": 1,
        ///           "statusId": 1
        ///           "due": "2020-11-15T15:20:51.699Z"
        ///       }
        ///   ]
        /// }
        ///
        ///
        /// </remarks>
        /// <response code="201">Newly created entities</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPost("bulk")]
        [ProducesResponseType(typeof(ICollection<Domain.Models.Task>), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult<Domain.Models.Task>> CreateTasks([FromBody, BindRequired] ICollection<CreateTaskRequest> taskRequests)
             => Created(string.Empty, await _taskRepository.CreateTasksAsync(taskRequests).ConfigureAwait(false));


        /// <summary>
        /// Updates a task
        /// </summary>
        /// <param name="taskRequest">
        /// Update Task request model
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/tasks
        ///     Body:
        ///       {
        ///           "id": 1,
        ///           "label": "Item1",
        ///           "description": "some description",
        ///           "due": "2020-11-15T15:20:51.699Z",
        ///           "listId": 1,
        ///           "statusId": 1,
        ///           "categoryId": 1
        ///       }
        ///
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> UpdateTask([FromBody, BindRequired] UpdateTaskRequest taskRequest)
        {
            await _taskRepository.UpdateTaskAsync(taskRequest).ConfigureAwait(false);
            return NoContent();
        }


        /// <summary>
        /// Updates the status of a task
        /// </summary>
        /// <param name="id">
        /// Task id
        /// </param>
        /// <param name="statusId">
        /// The Status Id
        /// </param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/tasks/{id}
        ///     Query:
        ///     statusId    
        /// 
        ///
        /// Sample response:
        /// 
        ///
        /// </remarks>
        /// <response code="204">No Content - operation was successful</response>
        /// <response code="400">If the request is faulty or the validation criteria are not met</response>  
        /// <response code="404">If the item does not exist</response>  
        /// <response code="500">In case of errors occuring on the data layer or anywhere on the server's side</response> 
        [HttpPatch("{id:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(ErrorResponse), 409)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> UpdateTaskStatus([FromRoute, BindRequired] int id, [FromQuery, BindRequired] int statusId)
        {
            await _taskRepository.UpdateTaskStatusAsync(id, statusId).ConfigureAwait(false);
            return NoContent();
        }
    }
}