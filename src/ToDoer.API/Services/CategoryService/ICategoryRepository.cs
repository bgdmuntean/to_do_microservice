using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.CategoryService
{
    public interface ICategoryRepository
    {
        Task<ICollection<Category>> GetAllCategoriesAsync();
        Task<Category> GetCategoryAsync(int id);
        Task<Category> CreateCategoryAsync(CreateCategoryRequest request);
        Task UpdateCategoryAsync(UpdateCategoryRequest request);
        Task DeleteCategoryAsync(int id);
    }
}