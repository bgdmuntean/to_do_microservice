using System.Data;
using System.Threading.Tasks;

namespace ToDoer.API.Services.DatabaseService
{
    public interface IDbConnectionRepository
    {
        Task<IDbConnection> CreateConnectionAsync();
    }
}