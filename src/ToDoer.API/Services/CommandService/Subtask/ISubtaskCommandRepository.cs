namespace ToDoer.API.Services.CommandService.Subtask
{
    public interface ISubtaskCommandRepository
    {
        string GetSubtasks { get; }
        string GetSubtasksById { get; }
        string GetSubtaskByTaskId {get;}
        string GetSubtasksBetweenDates {get;}
        string ChangeStatus {get;}
        string AddSubtask { get; }
        string UpdateSubtask { get; }
        string DeleteSubtask { get; } 
    }
}