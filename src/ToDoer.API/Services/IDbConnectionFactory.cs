using System.Data.Common;

namespace ToDoer.API.Services
{
    public interface IDbConnectionFactory
    {
        DbConnection Create(string connectionName);
    }
}