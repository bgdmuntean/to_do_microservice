using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.SubtaskService
{
    public interface ISubtaskRepository
    {
        Task<ICollection<Subtask>> GetAllSubtasksAsync();
        Task<ICollection<Subtask>> GetSubtasksFilteredAsync(FilterSubtaskRequest request);
        Task<Subtask> GetSubtaskAsync(int id);
        Task UpdateSubtaskStatusAsync(int id, int statusId);
        Task<ICollection<Subtask>> GetSubtaskByTaskIdAsync(int id);
        Task<Subtask> CreateSubtaskAsync(CreateSubtaskRequest request);
        Task<ICollection<Subtask>> CreateSubtasksAsync(ICollection<CreateSubtaskRequest> request);
        Task UpdateSubtaskAsync(UpdateSubtaskRequest request);
        Task DeleteSubtaskAsync(int id);
    }
}