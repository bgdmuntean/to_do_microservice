using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Todoer.API.Services;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;
using ToDoer.API.Domain.Requests;
using ToDoer.API.Services.CommandService.Subtask;
using ToDoer.API.Services.CommandService.Task;
using ToDoer.API.Utils.Extensions;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Services.SubtaskService
{
    public class SubtaskRepository : ISubtaskRepository
    {
        private readonly IMapper _mapper;
        private readonly ISubtaskCommandRepository _command;
        private readonly IBaseRepository _baseRepository;
        private readonly ITaskCommandRepository _taskCommand;
        public SubtaskRepository(IBaseRepository baseRepository, IMapper mapper, ISubtaskCommandRepository command, ITaskCommandRepository taskCommand)
        {
            _command = command;
            _baseRepository = baseRepository;
            _taskCommand = taskCommand;
            _mapper = mapper;
        }

        public async Task<Subtask> CreateSubtaskAsync(CreateSubtaskRequest request)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var task = await conn.QueryFirstOrDefaultAsync<Domain.Models.Task>(_taskCommand.GetTaskById, new {Id = request.TaskId}).ConfigureAwait(false);
                    CanAddTask(request, task);

                    var subtask = _mapper.Map<Subtask>(request);

                    subtask.CreatedAt = DateTimeOffset.UtcNow;
                    subtask.ModifiedAt = DateTimeOffset.UtcNow;
                    
                    var id = await conn.ExecuteAsync(_command.AddSubtask, subtask);

                    subtask = await conn.QueryFirstOrDefaultAsync<Subtask>(_command.GetSubtasksById, new {Id = id}).ConfigureAwait(false);
                    return subtask;    

                }
            ).ConfigureAwait(false);

    
        public async Task<ICollection<Subtask>> CreateSubtasksAsync(ICollection<CreateSubtaskRequest> request)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var subtasks = new List<Subtask>();
                    foreach(var subtaskReq in request)
                    {
                        var subtask = _mapper.Map<Subtask>(request);

                        subtask.CreatedAt = DateTimeOffset.UtcNow;
                        subtask.ModifiedAt = DateTimeOffset.UtcNow;

                        var id = await conn.ExecuteAsync(_command.AddSubtask, subtask);
                        subtasks.Add(await conn.QueryFirstOrDefaultAsync<Subtask>(_command.GetSubtasksById, new {Id = id}).ConfigureAwait(false));
                    }
                    return subtasks;
                }
            ).ConfigureAwait(false);

        public async System.Threading.Tasks.Task DeleteSubtaskAsync(int id)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var subtask = await conn.QueryFirstOrDefaultAsync<Subtask>(_command.GetSubtasksById, new {Id = id}).ConfigureAwait(false);
                    if(subtask == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Subtask with id [{id}] was not found.");
                    }

                    await conn.ExecuteAsync(_command.DeleteSubtask, new {Id = id}).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<Subtask>> GetAllSubtasksAsync()
            => await _baseRepository.WithConnection(async conn => 
                {
                    var subtasks = await conn.QueryAsync<Subtask>(_command.GetSubtasks).ConfigureAwait(false);
                    return subtasks.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<Subtask> GetSubtaskAsync(int id)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var subtask = await conn.QueryFirstOrDefaultAsync<Subtask>(_command.GetSubtasksById, new {Id = id}).ConfigureAwait(false);
                    if(subtask == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Subtask with id [{id}] was not found.");
                    }
                    return subtask;
                }
            ).ConfigureAwait(false);

        public async Task<ICollection<Subtask>> GetSubtaskByTaskIdAsync(int id)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var subtasks = await conn.QueryAsync<Subtask>(_command.GetSubtaskByTaskId, new {TaskId = id}).ConfigureAwait(false);
                    return subtasks.AsList();
                }
            ).ConfigureAwait(false);

        public async Task<ICollection<Subtask>> GetSubtasksFilteredAsync(FilterSubtaskRequest request)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var query = await conn.QueryAsync<Subtask>(_command.GetSubtasksBetweenDates, request).ConfigureAwait(false);
                    return query.AsList();
                }
            ).ConfigureAwait(false);

        public async System.Threading.Tasks.Task UpdateSubtaskAsync(UpdateSubtaskRequest request)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var subtask = await conn.QueryFirstOrDefaultAsync<Subtask>(_command.GetSubtasksById, new {Id = request.Id}).ConfigureAwait(false);
                    if(subtask == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Subtask with id [{request.Id}] was not found");
                    }

                    subtask = _mapper.Map<Subtask>(request);
                    subtask.ModifiedAt = DateTimeOffset.UtcNow;
                    await conn.ExecuteAsync(_command.UpdateSubtask, subtask).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);

        public async Task UpdateSubtaskStatusAsync(int id, int statusId)
            => await _baseRepository.WithConnection(async conn => 
                {
                    var subtask = await conn.QueryFirstOrDefaultAsync<Subtask>(_command.GetSubtasksById, new {Id = id}).ConfigureAwait(false);
                    if(subtask == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound, $"Subtask with id [{id}] was not found.");
                    }
                    await conn.ExecuteAsync(_command.ChangeStatus, new {Id = id, StatusId = statusId }).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);


        private static void CanAddTask(CreateSubtaskRequest request, Domain.Models.Task task)
        {
            if(task == null)
            {
                throw new HttpException(HttpStatusCode.NotFound, $"Subtask with id [{request.TaskId}] was not found");
            }

            if(request.IsValid(task))
            {
                throw new HttpException(HttpStatusCode.Forbidden, $"The due date of a subtask cannot be later than the due date of a task (Subtask: [{JsonConvert.SerializeObject(request)}], Task: [{JsonConvert.SerializeObject(new{ Label = task.Label, Due = task.Due, CreatedAt = task.CreatedAt, ModifiedAt = task.ModifiedAt})}])");
            }
        }
    }
}