using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace ToDoer.API.Services
{
    public class MssqlConnectionFactory : IDbConnectionFactory
    {
        private readonly IConfiguration _configuration;

        public MssqlConnectionFactory(IConfiguration configuration)
        {
            _configuration = configuration;   
        }
        public DbConnection Create(string connectionName)
        {
            return new SqlConnection(_configuration.GetConnectionString(connectionName));
        }
    }
}