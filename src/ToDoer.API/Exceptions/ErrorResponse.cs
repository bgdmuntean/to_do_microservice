using System.Net;
using Newtonsoft.Json;

namespace ToDoer.API.Exceptions
{
    public class ErrorResponse 
    {
        public HttpStatusCode StatusCode {get;set;} = HttpStatusCode.InternalServerError;
        public string Message {get; set;} = "An error has occured on the server side";
        

        public string ToJsonString()
        => JsonConvert.SerializeObject(this);
    }
}