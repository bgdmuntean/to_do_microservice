using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ToDoer.API.Exceptions;

namespace Todoer.API.Utils.Middleware
{
     public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        
        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(context, exception);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var errorResponse = new ErrorResponse();

            if (exception is HttpException httpException)
            {
                errorResponse.StatusCode = httpException.StatusCode;
                errorResponse.Message = httpException.Message;
            }
            else if(exception is DbInternalException databaseException)
            {
                errorResponse.StatusCode = databaseException.StatusCode;
                errorResponse.Message = databaseException.Message;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) errorResponse.StatusCode;
            await context.Response.WriteAsync(errorResponse.ToJsonString());
        }
    }
}