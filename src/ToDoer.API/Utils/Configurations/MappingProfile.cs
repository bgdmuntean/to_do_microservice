using AutoMapper;
using ToDoer.API.Domain.Models;
using ToDoer.API.Models;

namespace ToDoer.API.Utils.Configurations
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateCategoryModel, Category>();
            CreateMap<UpdateCategoryModel, Category>();

            CreateMap<CreateListModel, List>();
            CreateMap<UpdateListModel, List>();

            CreateMap<CreateStatusModel, Status>();
            CreateMap<UpdateStatusModel, Status>();

            CreateMap<CreateSubtaskModel, Subtask>();
            CreateMap<UpdateSubtaskModel, Subtask>();

            CreateMap<CreateTaskModel, Task>();
            CreateMap<UpdateTaskModel, Task>();
        }
    }
}