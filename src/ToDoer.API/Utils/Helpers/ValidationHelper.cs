namespace ToDoer.API.Utils.Helpers
{
    public static class ValidationHelpers
    {
        public static string CreateValidationErrorMessage(string entityName, string property, string reason)
            => $"Validation for entity [{entityName} failed due to [{property}] {reason}";
    }
}