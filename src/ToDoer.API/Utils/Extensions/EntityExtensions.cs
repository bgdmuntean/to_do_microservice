using System;
using ToDoer.API.Domain.Models;
using ToDoer.API.Models;

namespace ToDoer.API.Utils.Extensions
{
    public static class EntityExtensions
    {
        public static bool IsValid(this CreateTaskModel request, List list)
        {
            if(DateTimeOffset.Compare(request.Due, list.Due) > 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this CreateSubtaskModel request, Task task)
        {
            if(DateTimeOffset.Compare(request.Due, task.Due) > 0)
            {
                return false;
            }
            return true;
        }
    }
}