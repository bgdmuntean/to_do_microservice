using System;
using System.IO;
using System.Reflection;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Todoer.API.Services;
using ToDoer.API.Services.CategoryService;
using ToDoer.API.Services.CommandService.Category;
using ToDoer.API.Services.CommandService.List;
using ToDoer.API.Services.CommandService.Status;
using ToDoer.API.Services.CommandService.Subtask;
using ToDoer.API.Services.CommandService.Task;
using ToDoer.API.Services.DatabaseService;
using ToDoer.API.Services.ListService;
using ToDoer.API.Services.StatusService;
using ToDoer.API.Services.SubtaskService;
using ToDoer.API.Services.TaskService;
using ToDoer.API.Utils.Configurations;
using ToDoer.API.Utils.Extensions;

namespace ToDoer.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            AddFluentValidation(services);
            AddSwagger(services);
            AddDependencyInjection(services);
            AddAutoMapper(services);
            AddDbContext(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // global Exception handler
            app.UseGlobalExceptionHandler();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ToDoer API");
                c.DisplayRequestDuration();
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c => 
                {
                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Version = "v1",
                        Title = "ToDoer API",
                        Description = " A simple .NET Core API that encapsulates a bunch of practices/technologies",
                        Contact = new OpenApiContact
                        {
                            Name = "Bogdan Muntean",
                            Email = "bgdmuntean@hotmail.com"
                        }
                    });

                    // Set the comments path for the Swagger JSON and UI.
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);
                }
            );
        }

        private static void AddDependencyInjection(IServiceCollection services)
        {
            // Dapper command repositories
            services.AddTransient<ICategoryCommandRepository, CategoryCommandRepository>();
            services.AddTransient<IListCommandRepository, ListCommandRepository>();
            services.AddTransient<IStatusCommandRepository, StatusCommandRepository>();
            services.AddTransient<ISubtaskCommandRepository, SubtaskCommandRepository>();
            services.AddTransient<ITaskCommandRepository, TaskCommandRepository>();

            // Services
            services.AddTransient<IBaseRepository, BaseRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IListRepository, ListRepository>();
            services.AddTransient<IStatusRepository, StatusRepository>();
            services.AddTransient<ISubtaskRepository, SubtaskRepository>();
            services.AddTransient<ITaskRepository, TaskRepository>();
        }

        private static void AddAutoMapper(IServiceCollection services)
        {
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        private static void AddFluentValidation(IServiceCollection services)
        {
            services.AddControllers(options => 
            {
                options.Filters.Add(new ValidationFilter());
            })
            .AddFluentValidation(options => 
            {
                options.RegisterValidatorsFromAssemblyContaining<Startup>();
            });
        }

        private void AddDbContext(IServiceCollection services)
            {
                services.AddTransient<IDbConnectionRepository>((sp) => new DbConnectionRepository(Configuration.GetConnectionString("TodoDatabase")));
            }
    }
}
