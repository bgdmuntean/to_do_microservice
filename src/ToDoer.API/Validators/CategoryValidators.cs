using FluentValidation;
using ToDoer.API.Models;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Validators
{
    public class CreateCategoryModelValidator: AbstractValidator<CreateCategoryModel>
    {
        public CreateCategoryModelValidator()
        {
            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateCategoryModel).ToString(), "Label", "being null or empty"));
        }
    }

    public class UpdateCategoryModelValidator : AbstractValidator<UpdateCategoryModel>
    {
        public UpdateCategoryModelValidator()
        {
            RuleFor(_ => _.Id)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateCategoryModel).ToString(), "Id", "being null or empty"));

            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateCategoryModel).ToString(), "Label", "being null or empty"));
        }
    }
}