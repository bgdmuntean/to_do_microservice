using FluentValidation;
using ToDoer.API.Models;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Validators
{
    public class CreateStatusModelValidator : AbstractValidator<CreateStatusModel>
    {
        public CreateStatusModelValidator()
        {
            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateStatusModel).ToString(), "Label", "being null or empty"));
        }
    }

    public class UpdatStatusModelValidator : AbstractValidator<UpdateStatusModel>
    {
        public UpdatStatusModelValidator()
        {
            RuleFor(_ => _.Id)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateStatusModel).ToString(), "Id", "being null or empty"));

            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateStatusModel).ToString(), "Label", "being null or empty"));
        }
    }
}