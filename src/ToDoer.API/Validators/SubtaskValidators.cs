using System;
using FluentValidation;
using ToDoer.API.Models;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Validators
{
    public class CreateSubtaskModelValidator : AbstractValidator<CreateSubtaskModel>
    {
        public CreateSubtaskModelValidator()
        {
            RuleFor(_ => _.TaskId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskModel), "TaskId", "being null or empty"));

            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskModel), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskModel), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskModel), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskModel), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateSubtaskModel), "Due", "being in the past"));
        }
    }


    public class UpdateSubtaskModelValidator : AbstractValidator<UpdateSubtaskModel>
    {
        public UpdateSubtaskModelValidator()
        {
            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateSubtaskModel), "Id", "being null or empty"));

            RuleFor(_ => _.TaskId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateSubtaskModel), "TaskId", "being null or empty"));

            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateSubtaskModel), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateSubtaskModel), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateSubtaskModel), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateSubtaskModel), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateSubtaskModel), "Due", "being in the past"));
        }
    }


        public class FilterSubtaskModelValidator : AbstractValidator<FilterSubtaskModel>
    {
        public FilterSubtaskModelValidator()
        {
            
            RuleFor(_ => _.StartDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskModel), "StartDate", "being null or empty"))
                .GreaterThanOrEqualTo(_ => _.EndDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskModel), "StartDate", "being greatar than EndDate"));

            RuleFor(_ => _.EndDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskModel), "EndDate", "being null or empty"))
                .LessThanOrEqualTo(_ => _.StartDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterSubtaskModel), "EndDate", "being smaller than StartDate"));
        }
    }
}