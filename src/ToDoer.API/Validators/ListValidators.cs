using FluentValidation;
using ToDoer.API.Models;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Validators
{
    public class CreateListModelValidator : AbstractValidator<CreateListModel>
    {
        public CreateListModelValidator()
        {
            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateListModel).ToString(), "Label", "being null or empty"));

             RuleFor(_ => _.Description)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateListModel).ToString(), "Description", "being null or empty"));

             RuleFor(_ => _.Due)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(CreateListModel).ToString(), "Due", "being null or empty"));
        }
    }

    public class UpdateListModelValidator : AbstractValidator<UpdateListModel>
    {
        public UpdateListModelValidator()
        {
            RuleFor(_ => _.Id)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateListModel).ToString(), "Id", "being null or empty"));

            RuleFor(_ => _.Label)
            .NotEmpty()
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(UpdateListModel).ToString(), "Label", "being null or empty"));
        }
    }

    public class FilterListModelValidator : AbstractValidator<FilterListModel>
    {
        public FilterListModelValidator()
        {
            RuleFor(_ => _.StartDate)
            .NotEmpty()
            .LessThanOrEqualTo(_ => _.EndDate) 
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(FilterListModel).ToString(), "StartDate", "being null or empty"));

            RuleFor(_ => _.EndDate)
            .NotEmpty()
            .GreaterThanOrEqualTo(_ => _.StartDate) 
            .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
            .WithMessage(ValidationHelpers.CreateValidationErrorMessage(typeof(FilterListModel).ToString(), "EndDate", "being null or empty"));
        }
    }
}