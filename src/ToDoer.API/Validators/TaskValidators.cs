using System;
using FluentValidation;
using ToDoer.API.Models;
using ToDoer.API.Utils.Constants;
using ToDoer.API.Utils.Helpers;

namespace ToDoer.API.Validators
{
    public class CreateTaskModelValidator : AbstractValidator<CreateTaskModel>
    {
        public CreateTaskModelValidator()
        {
            RuleFor(_ => _.ListId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskModel), "ListId", "being null or empty"));

            RuleFor(_ => _.CategoryId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskModel), "CategoryId", "being null or empty"));

            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskModel), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskModel), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskModel), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskModel), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(CreateTaskModel), "Due", "being in the past"));
        }
    }


    public class UpdateTaskModelValidator : AbstractValidator<UpdateTaskModel>
    {
        public UpdateTaskModelValidator()
        {
            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "Id", "being null or empty"));

            RuleFor(_ => _.ListId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "ListId", "being null or empty"));

            RuleFor(_ => _.CategoryId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "CategoryId", "being null or empty"));
            
            RuleFor(_ => _.StatusId)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "StatusId", "being null or empty"));

            RuleFor(_ => _.Label)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "Label", "being null or empty"));

            RuleFor(_ => _.Description)
                .NotEmpty()
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "Description", "being null or empty"));

            RuleFor(_ => _.Due)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "Due", "being null or empty"))
                .LessThanOrEqualTo(DateTimeOffset.UtcNow)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(UpdateTaskModel), "Due", "being in the past"));
        }
    }

        public class FilterTaskModelValidator : AbstractValidator<FilterTaskModel>
    {
        public FilterTaskModelValidator()
        {
            RuleFor(_ => _.StartDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskModel), "StartDate", "being null or empty"))
                .LessThanOrEqualTo(_ => _.EndDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskModel), "StartDate", "being greater than EndDate"));


              RuleFor(_ => _.StartDate)
                .NotEmpty()
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskModel), "EndDate", "being null or empty"))
                .GreaterThanOrEqualTo(_ => _.StartDate)
                .WithErrorCode(HttpConstants.StatusCodes.BadRequest)
                .WithMessage(ValidationHelpers.CreateValidationErrorMessage(nameof(FilterTaskModel), "EndDate", "being smaller than StartDate"));
        }
    }
}