using System;

namespace ToDoer.API.Models
{
    public class CreateSubtaskModel
    {
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
        public int TaskId {get;set;}
        public int StatusId {get;set;}
    }

    public class UpdateSubtaskModel
    {
        public int Id {get;set;}
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
        public int TaskId {get;set;}
        public int StatusId {get;set;}
    }

    public class FilterSubtaskModel
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}