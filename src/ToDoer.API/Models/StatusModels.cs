namespace ToDoer.API.Models
{
    public class CreateStatusModel
    {
        public string Label { get; set; }
    }

    public class UpdateStatusModel
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }
}