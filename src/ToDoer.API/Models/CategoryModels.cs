namespace ToDoer.API.Models
{
    public class CreateCategoryModel
    {
        public string Label { get; set; }
    }

    public class UpdateCategoryModel
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }
}