using System;

namespace ToDoer.API.Models
{
    public class CreateListModel
    {
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
    }

    public class UpdateListModel
    {
        public int Id {get;set;}
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Due { get; set; }
    }

    public class FilterListModel
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}