namespace ToDoer.API.Services.Commands.List
{
    public interface IListCommandRepository
    {
        string GetLists { get; }
        string GetListById { get; }
        string GetListsBetweenDates {get;}
        string AddList { get; }
        string UpdateList { get; }
        string DeleteList { get; } 
    }
}