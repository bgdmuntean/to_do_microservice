namespace ToDoer.API.Services.Commands.List
{
    public class ListCommandRepository : IListCommandRepository
    {
        public string GetLists => "SELECT * FROM List";
        public string GetListById => "SELECT * FROM List WHERE Id = @Id";
        public string GetListsBetweenDates => "SELECT * FROM List WHERE Due BETWEEN @StartDate and @EndDate";
        public string AddList => "INSERT INTO List (Label, Description, CreatedAt, ModifiedAt, Due) VALUES(@Label, @Description, @CreatedAt, @ModifiedAt, @Due)";
        public string UpdateList => "UPDATE List SET Label = @Label, Description = @Description, ModifiedAt = @ModifiedAt, Due = @Due WHERE Id = @Id";
        public string DeleteList => "DELETE FROM Task WHERE Id = @Id";
    }
}