namespace ToDoer.API.Services.Commands.Category
{
    public class CategoryCommandRepository : ICategoryCommandRepository
    {
        public string GetCategories => "SELECT * FROM Category";
        public string GetCategoryById => "SELECT * FROM Category WHERE Id = @Id";
        public string GetCategoryByLabel => "SELECT * FROM Category WHERE Label = @Label";
        public string AddCategory => "INSERT INTO Category (Label) VALUES(@Label)";
        public string UpdateCategory => "UPDATE Category SET Label = @Label WHERE Id = @Id";
        public string DeleteCategory => "DELETE FROM Category WHERE Id = @Id";
    }
}