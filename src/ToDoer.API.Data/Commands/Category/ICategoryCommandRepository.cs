namespace ToDoer.API.Services.Commands.Category
{
    public interface ICategoryCommandRepository
    {
        string GetCategories { get; }
        string GetCategoryById { get; }
        string GetCategoryByLabel {get;}
        string AddCategory { get; }
        string UpdateCategory { get; }
        string DeleteCategory { get; } 
    }
}