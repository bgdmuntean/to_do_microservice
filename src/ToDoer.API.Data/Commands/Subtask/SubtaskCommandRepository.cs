namespace ToDoer.API.Services.Commands.Subtask
{
    public class SubtaskCommandRepository : ISubtaskCommandRepository
    {
        public string GetSubtasks => "SELECT * FROM Subtask";
        public string GetSubtaskById => "SELECT * FROM Subtask WHERE Id = @Id";
        public string GetSubtasksByTaskId => "SELECT * FROM Subtask WHERE Task = @TaskId";
        public string AddSubtask => "INSERT INTO Subtask (Label, Description, CreatedAt, ModifiedAt, Due, TaskId, StatusId) VALUES(@Label, @Description, @CreatedAt, @ModifiedAt, @Due, @TaskId, @StatusId)";
        public string ChangeStatus => "UPDATE Subtask SET StatusId = @StatusId WHERE Id = @Id";
        public string UpdateSubtask => "UPDATE Subtask SET Label = @Label, Description = @Description, ModifiedAt = @ModifiedAt, Due = @Due, TaskId = @TaskId, StatusId = @StatusId WHERE Id = @Id";
        public string DeleteSubtask => "DELETE FROM Subtask WHERE Id = @Id";
        public string GetSubtasksBetweenDates => "SELECT * FROM Subtask WHERE Due BETWEEN @StartDate AND @EndDate";
    }
}
