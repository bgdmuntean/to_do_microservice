namespace ToDoer.API.Services.Commands.Subtask
{
    public interface ISubtaskCommandRepository
    {
        string GetSubtasks { get; }
        string GetSubtaskById { get; }
        string GetSubtasksByTaskId {get;}
        string GetSubtasksBetweenDates {get;}
        string ChangeStatus {get;}
        string AddSubtask { get; }
        string UpdateSubtask { get; }
        string DeleteSubtask { get; } 
    }
}