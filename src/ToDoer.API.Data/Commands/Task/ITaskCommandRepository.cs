namespace ToDoer.API.Services.Commands.Task
{
    public interface ITaskCommandRepository
    {
        string GetTasks { get; }
        string GetTaskById { get; }
        string GetTasksByListId { get; }
        string GetTasksBetweenDates { get; }
        string AddTask { get; }
        string ChangeStatus {get;}
        string UpdateTask { get; }
        string DeleteTask { get; } 
    }
}