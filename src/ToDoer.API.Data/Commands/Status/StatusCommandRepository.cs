namespace ToDoer.API.Services.Commands.Status
{
    public class StatusCommandRepository : IStatusCommandRepository
    {
        public string GetStatuses => "SELECT * FROM Status";
        public string GetStatusById => "SELECT * FROM Status WHERE Id = @Id";
        public string AddStatus => "INSERT INTO Status (Label) VALUES(@Label)";
        public string UpdateStatus => "UPDATE Status SET Label = @Label WHERE Id = @Id";
        public string DeleteStatus => "DELETE FROM Status WHERE Id = @Id";
        public string GetStatusByLabel => "SELECT * FROM Status WHERE Label = @Label";
    }
}