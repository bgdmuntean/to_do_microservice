namespace ToDoer.API.Services.Commands.Status
{
    public interface IStatusCommandRepository
    {
        string GetStatuses { get; }
        string GetStatusById { get; }
        string GetStatusByLabel { get; }
        string AddStatus { get; }
        string UpdateStatus { get; }
        string DeleteStatus { get; } 
        
    }
}