using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Subtasks
{
    public interface ISubtaskRepository
    {
        Task<ICollection<Subtask>> GetAllSubtasksAsync();
        Task<ICollection<Subtask>> GetSubtasksFilteredAsync(DateTimeOffset startDate, DateTimeOffset endDate);
        Task<Subtask> GetSubtaskAsync(int id);
        Task UpdateSubtaskStatusAsync(int id, int statusId);
        Task<ICollection<Subtask>> GetSubtaskByTaskIdAsync(int id);
        Task<int> CreateSubtaskAsync(Subtask subtask);
        Task UpdateSubtaskAsync(Subtask subtask);
        Task DeleteSubtaskAsync(int id);
    }
}