using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ToDoer.API.Domain.Models;
using ToDoer.API.Services.Commands.Subtask;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Subtasks
{
    public class SubtaskRepository : Repository<Subtask>, ISubtaskRepository
    {
        private readonly ISubtaskCommandRepository _command;

        public SubtaskRepository(IConfiguration configuration, ISubtaskCommandRepository command) : base(configuration)
            => _command = command;


        public async Task<int> CreateSubtaskAsync(Subtask subtask)
            => await AddAsync(subtask, _command.AddSubtask).ConfigureAwait(false);


        public async Task DeleteSubtaskAsync(int id)
            => await DeleteAsync(id, _command.DeleteSubtask).ConfigureAwait(false);


        public async Task<ICollection<Subtask>> GetAllSubtasksAsync()
            => await GetAllAsync(_command.GetSubtasks).ConfigureAwait(false);


        public async Task<Subtask> GetSubtaskAsync(int id)
            => await GetByIdAsync(id, _command.GetSubtaskById).ConfigureAwait(false);


        public async Task<ICollection<Subtask>> GetSubtaskByTaskIdAsync(int id)
            => await WithConnection(async conn => 
                {
                    var subtasks = await conn.QueryAsync<Subtask>(_command.GetSubtasksByTaskId, new {TaskId = id}).ConfigureAwait(false);
                    return subtasks.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<Subtask>> GetSubtasksFilteredAsync(DateTimeOffset startDate, DateTimeOffset endDate)
            => await WithConnection(async conn => 
                {
                    var subtasks = await conn.QueryAsync<Subtask>(_command.GetSubtasksBetweenDates, new {StartDate = startDate, EndDate = endDate}).ConfigureAwait(false);
                    return subtasks.AsList();
                }
            ).ConfigureAwait(false);

        public async Task UpdateSubtaskAsync(Subtask subtask)
            => await UpdateAsync(subtask, _command.UpdateSubtask).ConfigureAwait(false);


        public async Task UpdateSubtaskStatusAsync(int id, int statusId)
            => await WithConnection(async conn => 
                {
                    await conn.ExecuteAsync(_command.ChangeStatus, new {Id = id, StatusId = statusId}).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);

    }
}