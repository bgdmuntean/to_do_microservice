using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoer.API.Data.Repositories
{
    public interface IRepository<T> where T : class, new()
    {
        Task<int> AddAsync(T entity, string command);
        Task<ICollection<T>> GetAllAsync(string command);
        Task<T> GetByIdAsync(int id, string command);
        Task DeleteAsync(int id, string command);
        Task UpdateAsync(T entity, string command);
    }
}