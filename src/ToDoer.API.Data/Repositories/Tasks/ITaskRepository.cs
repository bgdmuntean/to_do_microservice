using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EntityTask = ToDoer.API.Domain.Models.Task;

namespace ToDoer.API.Data.Repositories.Tasks
{
    public interface ITaskRepository
    {
        Task<ICollection<EntityTask>> GetAllTasksAsync();
        Task<ICollection<EntityTask>> GetTasksFilteredAsync(DateTimeOffset startDate, DateTimeOffset endDate);
        Task<EntityTask> GetTaskAsync(int id);
        Task UpdateTaskStatusAsync(int id, int statusId);
        Task<ICollection<EntityTask>> GetTasksByListIdAsync(int id);
        Task<int> CreateTaskAsync(EntityTask task);
        Task UpdateTaskAsync(EntityTask task);
        Task DeleteTaskAsync(int id);
    }
}