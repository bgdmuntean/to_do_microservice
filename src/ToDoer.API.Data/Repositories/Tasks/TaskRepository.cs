using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ToDoer.API.Services.Commands.Task;
using EntityTask = ToDoer.API.Domain.Models.Task;


namespace ToDoer.API.Data.Repositories.Tasks
{
    public class TaskRepository : Repository<EntityTask>, ITaskRepository
    {
        private readonly ITaskCommandRepository _command;
        public TaskRepository(IConfiguration configuration, ITaskCommandRepository command) : base(configuration)
            => _command = command;


        public async Task<int> CreateTaskAsync(EntityTask task)
            => await AddAsync(task, _command.AddTask).ConfigureAwait(false);


        public async Task DeleteTaskAsync(int id)
            => await DeleteAsync(id, _command.DeleteTask).ConfigureAwait(false);


        public async Task<ICollection<EntityTask>> GetAllTasksAsync()
            => await GetAllAsync(_command.GetTasks).ConfigureAwait(false);


        public async Task<EntityTask> GetTaskAsync(int id)
            => await GetByIdAsync(id, _command.GetTaskById).ConfigureAwait(false); 


        public async Task<ICollection<EntityTask>> GetTasksByListIdAsync(int id)
            => await WithConnection(async conn => 
                {
                    var tasks = await conn.QueryAsync<EntityTask>(_command.GetTasksByListId, new {ListId = id}).ConfigureAwait(false);
                    return tasks.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<EntityTask>> GetTasksFilteredAsync(DateTimeOffset startDate, DateTimeOffset endDate)
            => await WithConnection(async conn => 
                {
                    var tasks = await conn.QueryAsync<EntityTask>(_command.GetTasksBetweenDates, new {StartDate = startDate, EndDate = endDate}).ConfigureAwait(false);
                    return tasks.AsList();
                }
            ).ConfigureAwait(false);


        public async Task UpdateTaskAsync(EntityTask task)
            => await UpdateAsync(task, _command.UpdateTask).ConfigureAwait(false);


        public async Task UpdateTaskStatusAsync(int id, int statusId)
            => await WithConnection(async conn => 
                {
                    await conn.ExecuteAsync(_command.ChangeStatus, new {Id = id, statusId = statusId}).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);

    }
}