using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ToDoer.API.Domain.Models;
using ToDoer.API.Services.Commands.List;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Lists
{
    public class ListRepository : Repository<List>, IListRepository
    {
        private readonly IListCommandRepository _command;
        public ListRepository(IConfiguration configuration, IListCommandRepository command) : base(configuration)
            => _command = command;
        

        public async Task<int> CreateListAsync(List list)
            => await AddAsync(list, _command.AddList).ConfigureAwait(false);


        public async Task DeleteListAsync(int id)
            => await DeleteAsync(id, _command.DeleteList).ConfigureAwait(false);


        public async Task<ICollection<List>> GetAllListsAsync()
            => await GetAllAsync(_command.GetLists).ConfigureAwait(false);


        public async Task<List> GetListAsync(int id)
            => await GetByIdAsync(id, _command.GetListById).ConfigureAwait(false);


        public async Task<ICollection<List>> GetListsFilteredAsync(DateTimeOffset startDate, DateTimeOffset endDate)
            => await WithConnection(async conn => 
                {
                    var lists = await conn.QueryAsync<List>(_command.GetListsBetweenDates, new {StartDate = startDate, EndDate = endDate}).ConfigureAwait(false);
                    return lists.AsList();
                }
            ).ConfigureAwait(false);


        public async Task UpdateListAsync(List list)
            => await UpdateAsync(list, _command.UpdateList).ConfigureAwait(false);
            
    }
}