using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Lists
{
    public interface IListRepository
    {
        Task<ICollection<List>> GetAllListsAsync();
        Task<List> GetListAsync(int id);
        Task<int> CreateListAsync(List list);
        Task<ICollection<List>> GetListsFilteredAsync(DateTimeOffset startDate, DateTimeOffset endDate);
        Task UpdateListAsync(List list);
        Task DeleteListAsync(int id);
    }
}