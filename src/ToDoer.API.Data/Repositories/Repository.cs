using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace ToDoer.API.Data.Repositories
{
    public abstract class Repository<T> : BaseRepository, IRepository<T> where T : class, new()
    {
        protected Repository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<int> AddAsync(T entity, string command)
            => await WithConnection(async conn => 
                {
                    var id = await conn.ExecuteAsync(command, entity).ConfigureAwait(false);
                    return id;
                }
            ).ConfigureAwait(false);


        public async Task DeleteAsync(int id, string command)
            => await WithConnection(async conn =>
                {
                    await conn.ExecuteAsync(command, new {Id = id}).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);


        public async Task<ICollection<T>> GetAllAsync(string command)
            => await WithConnection(async conn => 
                {
                    var query = await conn.QueryAsync<T>(command).ConfigureAwait(false);
                    return query.AsList();
                }
            ).ConfigureAwait(false);


        public async Task<T> GetByIdAsync(int id, string command)
            => await WithConnection(async conn => 
                {
                    var entity = await conn.QueryFirstOrDefaultAsync<T>(command, new {Id = id}).ConfigureAwait(false);
                    return entity;
                }
            ).ConfigureAwait(false);


        public async Task UpdateAsync(T entity, string command)
            => await WithConnection(async conn => 
                {
                    await conn.ExecuteAsync(command, entity).ConfigureAwait(false);
                }
            ).ConfigureAwait(false);
    }
}