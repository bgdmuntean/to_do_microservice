using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Statuses
{
    public interface IStatusRepository
    {
        Task<ICollection<Status>> GetAllStatusesAsync();
        Task<Status> GetStatusAsync(int id);
        Task<Status> GetStatusByLabelAsync(string label);
        Task<int> CreateStatusAsync(Status request);
        Task UpdateStatusAsync(Status request);
        Task DeleteStatusAsync(int id);
    }
}