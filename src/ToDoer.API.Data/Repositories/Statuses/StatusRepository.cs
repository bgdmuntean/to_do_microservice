using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ToDoer.API.Domain.Models;
using ToDoer.API.Services.Commands.Status;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Statuses
{
    public class StatusRepository : Repository<Status>, IStatusRepository
    {
        private readonly IStatusCommandRepository _command;

        public StatusRepository(IConfiguration configuration, IStatusCommandRepository command) : base(configuration)
            => _command = command;


        public async Task<int> CreateStatusAsync(Status status)
            => await AddAsync(status, _command.AddStatus).ConfigureAwait(false);


        public async Task DeleteStatusAsync(int id)
            => await DeleteAsync(id, _command.DeleteStatus).ConfigureAwait(false);


        public async Task<ICollection<Status>> GetAllStatusesAsync()
            => await GetAllAsync(_command.GetStatuses).ConfigureAwait(false);


        public async Task<Status> GetStatusAsync(int id)
            => await GetByIdAsync(id, _command.GetStatusById).ConfigureAwait(false);


        public async Task<Status> GetStatusByLabelAsync(string label)
            => await WithConnection(async conn => 
                {
                    var status = await conn.QueryFirstOrDefaultAsync<Status>(_command.GetStatusByLabel, new {Label = label}).ConfigureAwait(false);
                    return status;
                }
            ).ConfigureAwait(false);


        public async Task UpdateStatusAsync(Status status)
            => await UpdateAsync(status, _command.UpdateStatus).ConfigureAwait(false);
    }
}