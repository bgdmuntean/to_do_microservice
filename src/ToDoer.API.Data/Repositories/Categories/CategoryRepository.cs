using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ToDoer.API.Domain.Models;
using ToDoer.API.Services.Commands.Category;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Categories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {

        private readonly ICategoryCommandRepository _command;

        public CategoryRepository(IConfiguration configuration, ICategoryCommandRepository command) : base(configuration)
            => _command = command;
        

        public async Task<int> CreateCategoryAsync(Category category)
            => await AddAsync(category, _command.AddCategory).ConfigureAwait(false);


        public async Task DeleteCategoryAsync(int id)
            => await DeleteAsync(id, _command.DeleteCategory).ConfigureAwait(false);


        public async Task<ICollection<Category>> GetAllCategoriesAsync()
            => await GetAllAsync(_command.GetCategories).ConfigureAwait(false);


        public async Task<Category> GetCategoryAsync(int id)
            => await GetByIdAsync(id, _command.GetCategoryById).ConfigureAwait(false);

        public async Task<Category> GetCategoryByLabelAsync(string label)
            => await WithConnection(async conn => 
                {
                    var category = await conn.QueryFirstOrDefaultAsync<Category>(_command.GetCategoryByLabel, new {Label = label}).ConfigureAwait(false);
                    return category;
                }
            ).ConfigureAwait(false);

        public async Task UpdateCategoryAsync(Category category)
            => await UpdateAsync(category, _command.UpdateCategory).ConfigureAwait(false);
    }
}