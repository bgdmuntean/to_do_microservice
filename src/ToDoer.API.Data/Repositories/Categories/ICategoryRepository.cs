using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoer.API.Domain.Models;
using Task = System.Threading.Tasks.Task;

namespace ToDoer.API.Data.Repositories.Categories
{
    public interface ICategoryRepository
    {
        Task<ICollection<Category>> GetAllCategoriesAsync();
        Task<Category> GetCategoryByLabelAsync(string label);
        Task<Category> GetCategoryAsync(int id);
        Task<int> CreateCategoryAsync(Category category);
        Task UpdateCategoryAsync(Category category);
        Task DeleteCategoryAsync(int id);
    }
}