using System;
using System.Data;
using System.Threading.Tasks;

namespace ToDoer.API.Data.Repositories
{
    public interface IBaseRepository
    {
        Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData);
        Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process);
        Task WithConnection(Func<IDbConnection, Task> getData);
    }

}