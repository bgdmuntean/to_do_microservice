using System;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using ToDoer.API.Domain.Exceptions;

namespace ToDoer.API.Data.Repositories
{
    public abstract class BaseRepository : IBaseRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("TodoDatabase");
        }

        // use for buffered queries that return a type
        public async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                await using(var connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync().ConfigureAwait(false);
                    return await getData(connection).ConfigureAwait(false);
                }
            }
            catch(TimeoutException ex)
            {
                throw new DbInternalException($"WithConnection() experienced an SQL timeout", ex);
            }
            catch(SqlException ex)
            {
                throw new DbInternalException($"WithConnection() experienced an SQL exception", ex);
            }
        }

        public async Task WithConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                await using(var connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch(TimeoutException ex)
            {
                throw new DbInternalException($"WithConnection() experienced an SQL timeout", ex);
            }
            catch(SqlException ex)
            {
                throw new DbInternalException($"WithConnection() experienced an SQL exception", ex);
            }
        }
            
        public async Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                await using (var connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync().ConfigureAwait(false);
                    var data = await getData(connection).ConfigureAwait(false);
                    return await process(data).ConfigureAwait(false);
                }
            }
            catch(TimeoutException ex)
            {
                throw new DbInternalException($"WithConnection() experienced an SQL timeout", ex);
            }
            catch(SqlException ex)
            {
                throw new DbInternalException($"WithConnection() experienced an SQL exception", ex);
            }
        }
    }
}