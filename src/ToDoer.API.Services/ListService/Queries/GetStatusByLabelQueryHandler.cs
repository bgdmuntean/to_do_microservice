using System.Collections.Generic;
using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Queries
{
    public class GetCategoriesQuery : IRequest<ICollection<Category>>
    {
        
    }
}