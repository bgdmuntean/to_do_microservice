using System.Collections.Generic;
using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.StatusService.Queries
{
    public class GetStatusesQuery : IRequest<ICollection<Status>>
    {
        
    }
}