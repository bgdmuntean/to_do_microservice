using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoer.API.Data.Repositories.Statuses;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Queries
{
    public class GetStatusByIdQueryHandler : IRequestHandler<GetStatusByIdQuery, Category>
    {
        private readonly IStatusRepository _statusRepository;

        public GetStatusByIdQueryHandler(IStatusRepository statusRepository)
        {
            _statusRepository = statusRepository;
        }


        public async Task<Category> Handle(GetStatusByIdQuery request, CancellationToken cancellationToken)
        {
            var category = await _statusRepository.GetStatusAsync(request.Id).ConfigureAwait(false);

            if(category == null)
            {
                throw new HttpException(HttpStatusCode.NotFound, $"The status with id [{request.Id}] was not found.");
            }
            return category;
        }
    }
}