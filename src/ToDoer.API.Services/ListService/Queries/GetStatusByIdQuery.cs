using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.StatusService.Queries
{
    public class GetStatusByIdQuery : IRequest<Category>
    {
        public int Id { get; set; }
    }
}