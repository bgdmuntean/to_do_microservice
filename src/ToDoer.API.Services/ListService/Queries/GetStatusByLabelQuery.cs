using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.StatusService.Queries
{
    public class GetStatusByLabelQuery : IRequest<Status>
    {
        public string Label { get; set; }
    }
}