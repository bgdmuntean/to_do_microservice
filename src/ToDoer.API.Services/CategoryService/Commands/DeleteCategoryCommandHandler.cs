using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoer.API.Data.Repositories.Categories;
using ToDoer.API.Domain.Exceptions;

namespace ToDoer.API.Services.CategoryService.Commands
{
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand>
    {
        private readonly ICategoryRepository _categoryRepository;

        public DeleteCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Unit> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = await _categoryRepository.GetCategoryAsync(request.Id).ConfigureAwait(false);
            
            if(category == null)
            {
                throw new HttpException(HttpStatusCode.NotFound, $"Category with id [{request.Id}] could not be found");
            }

            await _categoryRepository.DeleteCategoryAsync(request.Id).ConfigureAwait(false);
            return Unit.Value;
        }

        
    }
}