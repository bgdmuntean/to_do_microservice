using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Commands
{
    public class CreateCategoryCommand : IRequest<Category>
    {
        public Category Category { get; set; }
    }
}