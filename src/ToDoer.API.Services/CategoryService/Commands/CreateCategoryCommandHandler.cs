using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using ToDoer.API.Data.Repositories.Categories;
using ToDoer.API.Domain.Exceptions;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Commands
{
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, Category>
    {
        private readonly ICategoryRepository _categoryRepository;

        public CreateCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Category> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = await _categoryRepository.GetCategoryByLabelAsync(request.Category.Label).ConfigureAwait(false);

            if(category != null)
            {
                throw new HttpException(HttpStatusCode.Conflict, $"A category already exists with the same properties [{JsonConvert.SerializeObject(category)}]");
            }

            var categoryId = await _categoryRepository.CreateCategoryAsync(request.Category).ConfigureAwait(false);
            category = await _categoryRepository.GetCategoryAsync(categoryId).ConfigureAwait(false);

            return category;
        }
    }
}