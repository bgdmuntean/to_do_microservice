using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Commands
{
    public class UpdateCategoryCommand : IRequest
    {
        public Category Category { get; set; }
    }
}