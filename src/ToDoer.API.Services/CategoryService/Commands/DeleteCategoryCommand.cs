using MediatR;

namespace ToDoer.API.Services.CategoryService.Commands
{
    public class DeleteCategoryCommand : IRequest
    {
        public int Id { get; set; }
    }
}