using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using ToDoer.API.Data.Repositories.Categories;
using ToDoer.API.Domain.Exceptions;

namespace ToDoer.API.Services.CategoryService.Commands
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand>
    {
        private readonly ICategoryRepository _categoryRepository;

        public UpdateCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Unit> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
             var category = await _categoryRepository.GetCategoryAsync(request.Category.Id).ConfigureAwait(false);

            if(category == null)
            {
                throw new HttpException(HttpStatusCode.NotFound, $"Category with id [{request.Category.Id}] could not be found");
            }

            await _categoryRepository.UpdateCategoryAsync(request.Category).ConfigureAwait(false);

            return Unit.Value;
        }
    }
}