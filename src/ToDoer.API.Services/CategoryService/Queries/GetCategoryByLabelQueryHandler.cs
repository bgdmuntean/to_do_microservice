using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoer.API.Data.Repositories.Categories;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Queries
{
    public class GetCategoryByLabelQueryHandler : IRequestHandler<GetCategoryByLabelQuery, Category>
    {
        private readonly ICategoryRepository _categoryRepository;
        public GetCategoryByLabelQueryHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Category> Handle(GetCategoryByLabelQuery request, CancellationToken cancellationToken)
            => await _categoryRepository.GetCategoryByLabelAsync(request.Label).ConfigureAwait(false);
    }
}