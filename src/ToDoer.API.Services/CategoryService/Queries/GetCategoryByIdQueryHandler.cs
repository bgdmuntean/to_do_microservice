using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoer.API.Data.Repositories.Categories;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Queries
{
    public class GetCategoryByIdQueryHandler : IRequestHandler<GetCategoryByIdQuery, Category>
    {
        private readonly ICategoryRepository _categoryRepository;
        public GetCategoryByIdQueryHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Category> Handle(GetCategoryByIdQuery request, CancellationToken cancellationToken)
            => await _categoryRepository.GetCategoryAsync(request.Id).ConfigureAwait(false);
    }
}