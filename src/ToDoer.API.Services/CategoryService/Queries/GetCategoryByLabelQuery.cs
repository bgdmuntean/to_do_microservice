using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Queries
{
    public class GetCategoryByLabelQuery : IRequest<Category>
    {
        public string Label { get; set; }
    }
}