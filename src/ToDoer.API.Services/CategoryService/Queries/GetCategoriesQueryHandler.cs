using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoer.API.Data.Repositories.Categories;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Queries
{
    public class GetCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, ICollection<Category>>
    {
        private readonly ICategoryRepository _categoryRepository;
        public GetCategoriesQueryHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<ICollection<Category>> Handle(GetCategoriesQuery request, CancellationToken cancellationToken)
            => await _categoryRepository.GetAllCategoriesAsync().ConfigureAwait(false);
    }
}