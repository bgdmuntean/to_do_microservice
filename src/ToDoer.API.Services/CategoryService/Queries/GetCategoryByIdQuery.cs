using MediatR;
using ToDoer.API.Domain.Models;

namespace ToDoer.API.Services.CategoryService.Queries
{
    public class GetCategoryByIdQuery : IRequest<Category>
    {
        public int Id { get; set; }
    }
}