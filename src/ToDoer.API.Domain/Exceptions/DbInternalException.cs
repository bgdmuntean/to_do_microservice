using System;
using System.Net;

namespace ToDoer.API.Domain.Exceptions
{
    public class DbInternalException : Exception
    {
        public HttpStatusCode StatusCode { get;}
        public DbInternalException(string message):base(message)
        {
            StatusCode = HttpStatusCode.InternalServerError;
        }

        public DbInternalException(string message, Exception ex):base(message, ex)
        {
            StatusCode = HttpStatusCode.InternalServerError;
        }
    }
}