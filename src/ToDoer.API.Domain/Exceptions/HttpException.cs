
using System;
using System.Net;

namespace ToDoer.API.Domain.Exceptions
{
    public class HttpException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public HttpException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpException(HttpStatusCode statusCode, string message, Exception ex) : base(message, ex)
        {
            StatusCode = statusCode;
        }
    }    
}