namespace ToDoer.API.Domain.Models
{
    public class Category
    {
        public int Id {get;set;}
        public string Label {get;set;}
    }
}