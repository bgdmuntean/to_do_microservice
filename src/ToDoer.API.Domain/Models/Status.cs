namespace ToDoer.API.Domain.Models
{
    public class Status 
    {
        public int Id {get;set;}
        public string Label {get;set;}
    }
}