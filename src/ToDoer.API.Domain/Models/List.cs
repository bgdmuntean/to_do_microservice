using System;

namespace ToDoer.API.Domain.Models
{
    public class List
    {
        public int Id {get;set;}
        public string Label {get;set;}
        public string Description {get;set;}
        public DateTimeOffset CreatedAt {get;set;}
        public DateTimeOffset ModifiedAt {get;set;}
        public DateTimeOffset Due {get;set;}
    }
}