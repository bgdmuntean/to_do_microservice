using System.Collections.Generic;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using Todoer.API.Services;
using ToDoer.API.Domain.Models;
using ToDoer.API.Services.CommandService.Status;
using ToDoer.API.Services.StatusService;
using Xunit;

namespace ToDoer.API.UnitTests
{
    public class StatusRepositoryTests
    {
        private readonly StatusRepository _sut;
        private readonly Mock<IStatusRepository> _statusMockRepo = new Mock<IStatusRepository>();
        private readonly Mock<IBaseRepository> _baseMockRepo = new Mock<IBaseRepository>();
        private readonly Mock<IStatusCommandRepository> _statusCommandMockRepo = new Mock<IStatusCommandRepository>();
        private readonly Mock<IMapper> _mapperMock = new Mock<IMapper>();
        

        public StatusRepositoryTests()
        {
            _sut = new StatusRepository(_baseMockRepo.Object, _statusCommandMockRepo.Object, _mapperMock.Object);
        }


        [Fact]
        public async System.Threading.Tasks.Task GetAllStatuses_ShouldReturnICollection_WhenStatusesExist()
        {
            // Arrange
            var statuses = new List<Status>
            {
                new Status
                {
                    Id = 1, 
                    Label = "Something"
                }, 
                new Status
                {
                    Id = 2, 
                    Label = "test"
                }
            };

            _statusMockRepo
                .Setup(x => x.GetAllStatusesAsync())
            .ReturnsAsync(statuses);

            // Act

            var statusesReturned = await _sut.GetAllStatusesAsync();

            // Assert
            Assert.Null(statusesReturned);
        }


        [Fact]
        public void GetAllStatuses_ShouldReturnEmptyICollection_WhenStatusesDontExist()
        {
            // Arrange

            // Act

            // Assert
        }
    }
}
